cd /home/$USER/
module load python/2.7.9/b2 blas hdf5/1.8.14 cuda cudnn/7.0 git
pip install scipy==0.17 --user
git clone git://github.com/Theano/Theano.git
cd Theano/
python setup.py develop --prefix=~/.local
pip install keras==1.0.3 --user
cd ../Documents/dsc530
